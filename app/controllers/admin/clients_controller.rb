class Admin::ClientsController < AdminController
  before_action :all_clients, only: [:index, :create, :update, :destroy]
  before_action :set_client, only: [:show, :edit, :update, :destroy]

  def index
  end

  def show
  end

  def new
    @client = Client.new
  end

  def create
    @client = Client.create(client_params)
  end

  def edit
  end

  def update
    @client.update_attributes(client_params)
  end

  def delete
    @client = Client.find(params[:client_id])
  end

  def destroy
    @client.destroy
  end

  private

  def set_client
    @client = Client.find(params[:id])
  end

  def all_clients
    @clients = Client.all
  end

  def client_params
    params.require(:client).permit(:id, :name, :url, :email, :phone, :image, :_destroy)
  end

end
