class Admin::DashboardController < AdminController
  before_action :all_clients, only: [:index]
  before_action :all_projects, only: [:index]
  before_action :all_quotes, only: [:index]

  def index
  end

  private

  def all_clients
    @clients = Client.all
  end

  def all_projects
    @projects = Project.all
  end

  def all_quotes
    @quotes = Quote.all
  end

end
