class Admin::ProjectsController < AdminController
  before_action :all_projects, only: [:index, :create, :update, :destroy]
  before_action :set_project, only: [:show, :edit, :update, :destroy]

  def index
  end

  def show
  end

  def new
    @project = Project.new
  end

  def create
    @project = Project.create(project_params)
  end

  def edit
  end

  def update
    @project.update_attributes(project_params)
  end

  def delete
    @project = Project.find(params[:project_id])
  end

  def destroy
    @project.destroy
  end

  private

  def set_project
    @project = Project.find(params[:id])
  end

  def all_projects
    @projects = Project.all
  end

  def project_params
    params.require(:project).permit(:id, :title, :date, :description, :featured, :url, :client_id, :image, :_destroy)
  end

end
