class Admin::QuotesController < AdminController
  before_action :all_quotes, only: [:index, :create, :update, :destroy]
  before_action :set_quote, only: [:show, :edit, :update, :destroy]

  def index
  end

  def show
  end

  def new
    @quote = Quote.new
  end

  def create
    @quote = Quote.create(quote_params)
  end

  def edit
  end

  def update
    @quote.update_attributes(quote_params)
  end

  def delete
    @quote = Quote.find(params[:quote_id])
  end

  def destroy
    @quote.destroy
  end

  private

  def set_quote
    @quote = Quote.find(params[:id])
  end

  def all_quotes
    @quotes = Quote.all
  end

  def quote_params
    params.require(:quote).permit(:id, :text, :client_id, :_destroy)
  end

end
