class ClientsController < ApplicationController
  before_action :all_clients, only: [:index]

  def index
  end

  private

  def all_clients
    @clients = Client.all
  end
end
