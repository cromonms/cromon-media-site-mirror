class ProjectsController < ApplicationController
  before_action :all_projects
  
  def index
  end

  private

  def all_projects
    @projects = Project.all
  end
end
