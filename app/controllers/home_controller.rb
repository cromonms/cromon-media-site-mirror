class HomeController < ApplicationController
  def index
    @quotes = Quote.all
  end

  def about
  end

  def labs
  end

  def contact
  end
end
