# Admin Controller, makes sure user is Admin before allowing access to Admin interface.
class AdminController < ApplicationController
  before_action :require_admin

  def index
  end

  private

  def require_admin
    unless admin_signed_in?
      redirect_to root_path
      flash[:notice] = "You must be a Site Administrator"
    end
  end
end
