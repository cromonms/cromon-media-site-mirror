# == Schema Information
#
# Table name: projects
#
#  id          :integer          not null, primary key
#  title       :string
#  date        :date
#  description :text
#  featured    :boolean
#  url         :string
#  client_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Project < ActiveRecord::Base
  belongs_to :client
  accepts_nested_attributes_for :client

  has_attached_file :image, styles: { large: "1000x1000>", medium: "600x600>", thumb: "200x200>", icon: "60x60>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
end
