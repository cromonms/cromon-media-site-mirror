require 'rails_helper'

RSpec.describe Admin::DashboardController, :type => :controller do
  describe "anonymous user" do
    before :each do
      # This simulates an anonymous user
      login_with nil
    end

    it "should be redirected to root path" do
      get :index
      expect( response ).to redirect_to( root_path )
    end
  end

  it "should let a user see all the posts" do
    login_with create( :admin )
    get :index
    expect( response ).to render_template( :index )
  end
end
