class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.date :date
      t.text :description
      t.boolean :featured
      t.string :url
      t.references :client, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
