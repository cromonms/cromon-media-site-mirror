Rails.application.routes.draw do
  devise_for :admins

  root to: 'home#index'
  get '/about', to: 'home#about'
  get '/labs', to: 'home#labs'
  get '/contact', to: 'home#contact'

  resources :projects, only: [:index]
  resources :clients, only: [:index]

  namespace :admin do
    get 'dashboard', to: 'dashboard#index'
    resources :clients do
      get "delete"
    end
    resources :projects do
      get "delete"
    end
    resources :quotes do
      get "delete"
    end
  end
end
